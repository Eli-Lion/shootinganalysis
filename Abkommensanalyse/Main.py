# Main Program
# imports
from Shots import Shot

# Filling Arrays function
def fill_arrays(real_shots, predicted_shots):
    # Explaination
    print("You will be asked what you think you scored first. Enter this without looking at the real result. You will be asked for this separately.")
    print("You will always be asked to enter the rings first and the sector second.")

    # Fill Arrays with user inputs
    for i in range(real_shots.num_of_shots):
        predicted_shots.rings[i] = float(input("How many rings do you think you scored? "))
        predicted_shots.sector[i] = int(input("Which sector do you think you hit? "))
        real_shots.rings[i] = float(input("How many rings did you score? "))
        real_shots.sector[i] = int(input("Which sector did you hit? "))
        print("")

# Function to print results
def print_results(ring_score, sector_score, total_score, percent_rings, percent_sectors, percent_score):
    # Print Scores
    print("Ring Score: ", ring_score)
    print("Sector Score: ", sector_score)
    print("Total Score: ", total_score)

    # Print Percentages
    print("Percentage of Rings correct: %.2f" % percent_rings)
    print("Percentage of Sectors correct: %.2f" % percent_sectors)
    print("Total Score in Percent: %.2f" % percent_score)
    return

# Main Function
def main():
    # setup the real and the predicted shots
    num_of_shots = int(input("How many shots do you want to make? "))
    real_shots = Shot([-1 for i in range(num_of_shots)], [-1 for i in range(num_of_shots)], num_of_shots)
    predicted_shots = Shot([-1 for i in range(num_of_shots)], [-1 for i in range(num_of_shots)], num_of_shots)
    num_of_sectors = int(input("Do you want 4 or 8 sectors? Type the number: "))
    mode = int(input("Choose a mode: 1 for full rings, 2 for half rings or 3 for 1/10 rings: "))

    # Fill arrays, adjust inputs, calculate scores
    fill_arrays(real_shots, predicted_shots)
    real_shots.adjust(mode)
    predicted_shots.adjust(mode)
    ring_score, sector_score, total_score = real_shots.calc_scores(predicted_shots)

    # Calculate Percentage
    percent_rings = ring_score / real_shots.num_of_shots * 100
    percent_sectors = sector_score / real_shots.num_of_shots * 100
    percent_score = (percent_rings + percent_sectors) / 2

    # print everything
    print_results(ring_score, sector_score, total_score, percent_rings, percent_sectors, percent_score)
    return

main()