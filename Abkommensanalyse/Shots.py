# Class for shots to make the main programm more readable
class Shot:

    # Initialise member values
    def __init__(self, rings, sector, num_of_shots):
        self.rings = rings
        self.sector = sector
        self.num_of_shots = num_of_shots


    # Function to adjust input to mode
    def adjust(self, mode):
        for i in range(self.num_of_shots):
            round(self.rings[i], 1)
        if mode == 1:
            for i in range(self.num_of_shots):
                self.rings[i] = int(self.rings[i])
        elif mode == 2:
            for i in range(self.num_of_shots):
                if self.rings[i] % 1 < 0.5:
                    self.rings[i] = self.rings[i] // 1
                else:
                    self.rings[i] = self.rings[i] // 1 + 0.5

    # Function to calculate scores
    def calc_scores(self, other):
        # Calculate Scores
        ring_score = 0
        sector_score = 0
        for i in range(self.num_of_shots):
            if self.rings[i] == other.rings[i]:
                ring_score += 1
            if self.sector[i] == other.sector[i]:
                sector_score += 1
        total_score = ring_score + sector_score
        # return the scores
        return ring_score, sector_score, total_score

